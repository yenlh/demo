from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver import Chrome
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import Select
from selenium import *
import sys
import argparse
import unittest
import time
import json

chrome_options = Options()
#chrome_options.add_argument('--headless')
#chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--disable-dev-shm-usage')
chrome_options.add_argument('--disable-web-security')

EXECUTOR = {
    'youmed': 'https://datkham.test.davido.vn/|https://datkham.test.davido.vn/user/login'
}

parser = argparse.ArgumentParser()
parser.add_argument('-e', '--executor', type=str, choices=EXECUTOR)
args = vars(parser.parse_args())
actual_url = EXECUTOR[args.executor].split("|")[0]
login_url = EXECUTOR[args.executor].split("|")[1]
# actual_url = 'https://datkham.test.davido.vn/'
# login_url = 'https://datkham.test.davido.vn/user/login'

class SearchText(unittest.TestCase):
    def setUp(self):
        # create a new Chrome session
        self.driver = webdriver.Chrome(executable_path='D:\Youmed\chromedriver', chrome_options=chrome_options)
        #self.driver = webdriver.Chrome('/usr/bin/chromedriver',chrome_options=chrome_options)
        self.driver.implicitly_wait(30)
        self.driver.maximize_window()
        # navigate to the application login page
        self.driver.get(actual_url)
        self.driver.find_element_by_css_selector(".btn-default").click()
        self.driver.implicitly_wait(5)

    def test_search_by_text(self):
        # enter phone number and submit
        time.sleep(1)
        self.driver.find_element_by_id("mat-input-0").click()
        self.driver.find_element_by_id("mat-input-0").send_keys("0587080907")
        time.sleep(1)
        self.driver.find_element_by_css_selector(".btn-primary").click()

        # enter otp request
        time.sleep(1)
        self.driver.find_element_by_id("mat-input-1").click()
        self.driver.find_element_by_id("mat-input-1").send_keys("000000")
        time.sleep(1)
        self.driver.find_element_by_css_selector(".btn-primary").click()
        
        # find provider
        self.driver.find_element_by_css_selector(".form-control").click()
        time.sleep(1)
        self.driver.find_element_by_css_selector(".form-control").send_keys("le dang quang")
        time.sleep(1)
        self.driver.find_element_by_css_selector(".input-group-append > .btn").click()
        time.sleep(1)
        self.driver.execute_script("window.scrollBy(0,250)","")
        time.sleep(5)
        self.driver.find_element_by_xpath('/html/body/app-root/app-owner-list-page/div[2]/div/div/div/div/div[2]/div[1]/div/app-booking-mark/button').click()
        time.sleep(1)

        # create an appointment
        self.driver.find_element_by_css_selector("#datetimepicker > div > button").click()
        time.sleep(1)
        self.driver.find_element_by_css_selector(".ngb-dp-week:nth-child(4) > .ngb-dp-day:nth-child(2) > .custom-day").click()
        time.sleep(1)
        cbx = self.driver.find_element_by_css_selector(".choose-time > .form-control")
        cbx.click()
        select = Select(cbx)
        select.select_by_visible_text('11:00-11:30')
        cbx.click()
        time.sleep(1)
        self.driver.find_element_by_xpath("/html/body/app-root/app-book/div/div[3]/ngb-tabset/div/div[1]/app-booking-self/div[1]/form/div[2]/div/div[1]").click()
        time.sleep(2)
        self.driver.execute_script("window.scrollBy(0,250)","")
        time.sleep(5)
        self.driver.find_element_by_css_selector("#SelfBooking-panel > app-booking-self > div.info-register > form > div.wrap-btn > button.btn.btn-primary.btn-next").click()
        time.sleep(2)
        self.driver.execute_script("window.scrollBy(0,1000)","")
        time.sleep(5)
        self.driver.find_element_by_css_selector("body > app-root > app-confirm > div > div.container > div > div.col-12.col-lg-7.left > div.group.end > div.row.payment > div:nth-child(2) > button > span").click()
        time.sleep(5)

        # compare successful login page with actual page
        time.sleep(1)
        success_login_page = self.driver.current_url
        print("Successful login page url: ", success_login_page)
        print("Page to compare with: ", actual_url)
        self.assertEqual(actual_url, success_login_page, 'Test login failed')


    def tearDown(self):
        # close the browser window
        self.driver.quit()

if __name__ == '__main__':
    unittest.main()

#TestCase
